# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y01
Title=Cross Section for $e^+e^-\to D^0+\bar{D}^0+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^0+\bar{D}^0+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y02
Title=Cross Section for $e^+e^-\to D^++D^-+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^++D^-+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y03
Title=Cross Section for $e^+e^-\to D_s^++D_s^-+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D_s^++D_s^-+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y04
Title=Cross Section for $e^+e^-\to \Lambda_c^++\bar{\Lambda}_c^-+X$
XLabel=
YLabel=$\sigma(e^+e^-\to \Lambda_c^++\bar{\Lambda}_c^-+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y05
Title=Cross Section for $e^+e^-\to D^{*0}+\bar{D}^{*0}+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^{*0}+\bar{D}^{*0}+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y06
Title=Cross Section for $e^+e^-\to D^{*+}+D^{*-}+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^{*+}+D^{*-}+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y07
Title=Cross Section for $e^+e^-\to D^{*+}+D^{*-}+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^{*+}+D^{*-}+X)$ [pb]
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d01-x01-y08
Title=Cross Section for $e^+e^-\to D^{*+}+D^{*-}+X$
XLabel=
YLabel=$\sigma(e^+e^-\to D^{*+}+D^{*-}+X)$ [pb]
# END PLOT

# BEGIN PLOT /BELLE_2006_S6265367/d02-x01-y01
Title=$D^{*+}\to D^0\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d03-x01-y01
Title=$D^0\to K^-\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d04-x01-y01
Title=$D^+\to K^-\pi^+\pi^-$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d05-x01-y01
Title=$D_s^+\to \phi\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d06-x01-y01
Title=$\Lambda_c^+\to p^+K^-\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d07-x01-y01
Title=$D^{*+}\to D^+\pi^0$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d08-x01-y01
Title=$D^{*0}\to D^0\pi^0$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d09-x01-y01
Title=$D^{*+}\to D^0\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d10-x01-y01
Title=$D^0\to K^-\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d11-x01-y01
Title=$D^+\to K^-\pi^+\pi^-$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d12-x01-y01
Title=$D_s^+\to \phi\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d13-x01-y01
Title=$\Lambda_c^+\to p^+K^-\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d14-x01-y01
Title=$D^{*+}\to D^+\pi^0$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d15-x01-y01
Title=$D^{*0}\to D^0\pi^0$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$s\times\text{d}\sigma/\text{d}x_p$ [$\text{GeV}^2$nb]
LogY=0
# END PLOT




# BEGIN PLOT /BELLE_2006_S6265367/d02-x01-y02
Title=$D^{*+}\to D^0\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d03-x01-y02
Title=$D^0\to K^-\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d04-x01-y02
Title=$D^+\to K^-\pi^+\pi^-$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d05-x01-y02
Title=$D_s^+\to \phi\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d06-x01-y02
Title=$\Lambda_c^+\to p^+K^-\pi^+$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d07-x01-y02
Title=$D^{*+}\to D^+\pi^0$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d08-x01-y02
Title=$D^{*0}\to D^0\pi^0$ scaled momentum in the continuum region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d09-x01-y02
Title=$D^{*+}\to D^0\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d10-x01-y02
Title=$D^0\to K^-\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d11-x01-y02
Title=$D^+\to K^-\pi^+\pi^-$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d12-x01-y02
Title=$D_s^+\to \phi\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d13-x01-y02
Title=$\Lambda_c^+\to p^+K^-\pi^+$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d14-x01-y02
Title=$D^{*+}\to D^+\pi^0$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
# BEGIN PLOT /BELLE_2006_S6265367/d15-x01-y02
Title=$D^{*0}\to D^0\pi^0$ scaled momentum in the resonance region
XLabel=$x_p$
YLabel=$1/\sigma\text{d}\sigma/\text{d}x_p$ 
LogY=0
# END PLOT
