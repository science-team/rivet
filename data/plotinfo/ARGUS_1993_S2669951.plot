# BEGIN PLOT /ARGUS_1993_S2669951/d02-x01-y01
Title=$f_0$ scaled momentum, continuum
XLabel=$x_p$ 
YLabel=$1/(\sigma\beta)\text{d}\sigma/\text{d}x_p$
# END PLOT
# BEGIN PLOT /ARGUS_1993_S2669951/d03-x01-y01
Title=$f_0$ scaled momentum, $\Upsilon(1S)$
XLabel=$x_p$ 
YLabel=$1/(\sigma\beta)\text{d}\sigma/\text{d}x_p$
# END PLOT
# BEGIN PLOT /ARGUS_1993_S2669951/d04-x01-y01
Title=$f_0$ scaled momentum,  $\Upsilon(2S)$
XLabel=$x_p$ 
YLabel=$1/(\sigma\beta)\text{d}\sigma/\text{d}x_p$
# END PLOT
# BEGIN PLOT /ARGUS_1993_S2669951/d01-x01-y01
Title=$\eta'$ multiplicity, $x_p>0.35$
XLabel=
YLabel=$N_{\eta'}$
LogY=0
# END PLOT
# BEGIN PLOT /ARGUS_1993_S2669951/d01-x01-y02
Title=$\eta'$ multiplicity, $x_p>0.35$
XLabel=
YLabel=$N_{\eta'}$
LogY=0
# END PLOT
# BEGIN PLOT /ARGUS_1993_S2669951/d05-x01-y01
Title=$f_0$ multiplicity
XLabel=
YLabel=$N_{f_0}$
LogY=0
# END PLOT
